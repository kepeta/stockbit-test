## Internet Gateway
resource "aws_internet_gateway" "test-igw" {
    vpc_id = "${aws_vpc.test-vpc.id}"

    tags {
        Name = "test-igw"
    }
}

## NAT Gateway
resource "aws_nat_gateway" "nat-private-subnet" {
  allocation_id = aws_eip.nat-private-subnet
  subnet_id     = aws_subnet.test-subnet-private-1

  tags = {
    Name = "test-nat-gw"
  }
  
  depends_on = [aws_internet_gateway.example]
}

## custom route table public subnets
resource "aws_route_table" "test-public-crt" {
    vpc_id = "${aws_vpc.test-vpc.id}"
    route {
        cidr_block = "0.0.0.0/0" 
        gateway_id = "${aws_internet_gateway.test-igw.id}" 
    }

    tags = {
        Name = "test-public-crt"
    }
}

# route table association for the public subnets
resource "aws_route_table_association" "test-crta-public-subnet-1" {
    subnet_id = "${aws_subnet.test-subnet-public-1.id}"
    route_table_id = "${aws_route_table.test-public-crt.id}"
}

## security group
resource "aws_security_group" "ssh-sg" {

    vpc_id = "${aws_vpc.test-vpc.id}"

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "allow-ssh-http-protocol"
    }
}
