variable "AWS_REGION" {
  default = "ap-southeast-1"
}

variable "PRIVATE_KEY_PATH" {
  default = "aws-test-keypair"
}

variable "PUBLIC_KEY_PATH" {
  default = "aws-test-keypair.pub"
}

variable "EC2_USER" {
  default = "ubuntu"
}

variable "AMI" {
  type = "map"

  default {
    ap-southeast-1 = "ami-0d058fe428540cd89"
  }
}