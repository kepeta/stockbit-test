resource "aws_vpc" "test-vpc" {
    cidr_block = "10.0.0.0/16"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    instance_tenancy = "default"

    tags {
        Name = "test-vpc"
    }
}

resource "aws_subnet" "test-subnet-public-1" {
    vpc_id = "${aws_vpc.test-vpc.id}"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true" // public subnet
    availability_zone = "ap-southeast-1b"

    tags {
        Name = "test-subnet-public-1"
    }

}

resource "aws_subnet" "test-subnet-private-1" {
    vpc_id = "${aws_vpc.test-vpc.id}"
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "false" // private subnet
    availability_zone = "ap-southeast-1b"

    tags {
        Name = "test-subnet-private-1"
    }
}

resource "aws_launch_configuration" "asg-launch-config-test" {
  instance_type = "t2.medium"
  security_groups = [aws_security_group.ssh-sg.id]
  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "test-asg-policy" {
  name                   = "test-asg-policy"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.test-asg.name
}

resource "aws_autoscaling_group" "test-asg" {
  launch_configuration = aws_launch_configuration.asg-launch-config-test.id
  name                 = test-asg
  availability_zones   = data.aws_availability_zones.all.names
  min_size = 2
  max_size = 5

  tag {
    key                 = "Name"
    value               = "test-asg-ec2"
    propagate_at_launch = true
  }
}

